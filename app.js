//const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const expressSanitizer = require('express-sanitizer')


var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
//var mongoose = require('mongoose');
var session=  require('express-session');
var fileupload=  require('express-fileupload');

/*
var express = require('express');
var app = express.Router();
*/

var app= express();


//const app = express();

//CONECTING DB// APP CONFI
mongoose.connect('mongodb://127.0.0.1:27017/Affinity-DB', {
    useNewUrlParser: true, 
    useCreateIndex: true,
    useFindAndModify: false
  });
app.use(bodyParser.urlencoded({extended:true}));
app.use(expressSanitizer());
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(methodOverride('_method'))

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret:'keyboard cat', resave:false,saveUninitialized:true}));
app.use(fileupload());

var schema=mongoose.Schema;

//SCHEMA
let blogSchema = mongoose.Schema({
    title: String,
    image: {
        type: String,
        default: 'imagePlaceholder.jpg' 
    },
    body: String,
    created: {
        type: Date,
        default: Date.now
    }
});

//MODEL

let Blog = mongoose.model('Blog', blogSchema)

var admin_data  = new schema({
    Admin_id   : String,
    Admin_name  : String,
    Admin_contact_no  : Number,
    Admin_email       : String,
    Admin_password    : String, 
});

var AdminRegistrationModel=mongoose.model('Admin',admin_data);

var admin_data  = new schema({
    Admin_id   : String,
    Admin_name  : String,
    Admin_contact_no  : Number,
    Admin_email       : String,
    Admin_password    : String, 
});

var AdminLoginModel=AdminRegistrationModel;

var category  = new schema({
    cate_id     : String,
    cate_name   : String  
});

var CategoryModel=mongoose.model('Category',category);

var customer_data  = new schema({

    Customer_id              :     String,
    Customer_name            :     String,
    Customer_contact_no      :     Number,
    Customer_email           :     String,
    Customer_address         :     String,
    Customer_dob             :     Date,
    Customer_gender          :     String, 
    Customer_password        :     String,
});
var CustomerModel=mongoose.model('Customer',customer_data);

var investing_data  = new schema({
    video   : String,
    description  : String,
    description2 : String,
    consistenttxt1  : String,
    consistenttxt2  : String,
    sustaintxt1  : String,
    sustaintxt2 : String,
    robusttxt1  : String,
    robusttxt2  : String,
    image_upload : String
});
var InvestingModel=mongoose.model('investing',investing_data);


var conn =mongoose.Collection;

var uploadSchema =new mongoose.Schema({
  imagename: String,

});

var uploadModel = mongoose.model('uploadimage', uploadSchema);
module.exports=uploadModel;


//SAMPLE BLOG
// Blog.create({
//     title: 'My first Academy',
//     image: 'https://images.unsplash.com/photo-1551517725-b926592c4280?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
//     body: 'Hello this is a blog post'
// })

app.get('/', function(req, res, next) {
    res.render('admin_login');
});


//RESTFUL ROUTES
app.get('/', (req, res) => {
    res.redirect('/blogs')
})


//INDEX ROUTES

app.get('/blogs', (req, res) => {
    //RETRIEVING ALL BLOGS
    Blog.find({}, (error, blogs) => {
      if(error){
          console.log(error);
      }else{
        res.render('blog/index', {blogs: blogs})
      }
    })
    
})

//NEW ROUTE
app.get('/blogs/new', (req, res) => {
    res.render('blog/new')
})


//NEW ROUTE
/*app.get('/admin/registration', (req, res) => {
    res.render('Admin/Admin_registration')
})

app.get('/admin/registration', (req, res) => {
    res.render('Admin/Admin_registration')
})
*/

/*
app.get('/investing/add', function(req, res, next) {
    res.render('Investing/Investing_add');
});
*/
/*
app.get('/', function(req, res, next) {
    res.render('admin_login');
});
*/

app.get('/dashboard', function(req, res, next) {
  InvestingModel.find(function(err,db_investing_array){
  CustomerModel.find(function(err,db_customer_array){

  CategoryModel.find(function(err,db_category_table){
    if(err){
      console.log("error");
    }else{
        res.render('dashboard',{investing_array:db_investing_array,customer_array:db_customer_array,category_array:db_category_table});
      }
    });
  });
});
});



//CREATE
app.post('/blogs', (req, res) => {
    //create blog
    Blog.create(req.body.blog, (error, newBlog) => {
      if(error){
          res.render('blog/new')
      }else{
           //redirect to index page
          res.redirect('/blogs')
      }
    })
   
})
//SHOW ROUTE
app.get('/blogs/:id', (req, res) => {
    Blog.findById(req.params.id, (error, foundBlog) => {
      if(error){
        res.redirect('/blogs')
      }else{
        res.render('blog/show', {blog:foundBlog})
      }
    })
});

//EDIT
app.get('/blogs/:id/edit', (req, res) => {
  Blog.findById(req.params.id, (error, foundBlog)=>{
    if(error){
      res.redirect('/blogs')
    }else {
      res.render('blog/edit', {blog:foundBlog})
    }
  })
});

//UPDATE ROUT
app.put('/blogs/:id', (req, res) => {
  Blog.findByIdAndUpdate(req.params.id, req.body.blog, (error, updatedBlog)=> {
    if(error) {
      res.redirect('/blogs')
    }else{
      res.redirect('/blogs/' + req.params.id)
    }
  })
});


//DELETE ROUTE
app.delete('/blogs/:id', (req, res) =>{
  //DESTROY BLOG
  Blog.findByIdAndRemove(req.params.id, (error)=> {
    if(error){
      res.redirect('/blogs')
    }else{
      res.redirect('/blogs')
    }
  })
});


app.get('/admin/registration', function(req, res, next) {
    res.render('Admin/Admin_registration');
});
app.post('/admin/registration',function(req,res,next){
console.log(req.body);
const admindata={
    Admin_id:           req.body.Admin_id,
    Admin_name:         req.body.Admin_name,
    Admin_contact_no:   req.body.Admin_contact_no,
    Admin_email:        req.body.Admin_email,
    Admin_password:     req.body.Admin_password
}
var registrationdata=AdminRegistrationModel(admindata);  
registrationdata.save(function(err){
        if(err)
        console.log("Error");
        else
        res.redirect('/admin/data_display');
    });
});
//Admin Registration

//display data
app.get('/admin/data_display',function(req,res,next){
    var mysessionvalue= req.session.Admin_email;
    //var mysessionvalue= "aa2@gmail.com";
    if(!mysessionvalue){
        res.redirect('/');
    }
    AdminRegistrationModel.find(function(err,db_admin_array){
        if(err)
        console.log("Error");
        else
        {
        console.log(db_admin_array);
    res.render('Admin/Admin_display',{admin_array:db_admin_array});
       
        }
    });
});
//display data

//delete data
app.get('/admin/delete/:id',function(req,res){
    var mysessionvalue= req.session.Admin_email;
    if(!mysessionvalue){
        res.redirect('/');
    }
    AdminRegistrationModel.findByIdAndDelete(req.params.id,function(err,db_admin_array){
        if(err)
        console.log("Error");
        else
        {
            console.log("-------------------------");
            console.log(db_admin_array);
            res.redirect('/admin/data_display');
        }
    });
});
//delete data

//edit data
app.get('/admin/edit/:id',function(req,res,next){
    var mysessionvalue= req.session.Admin_email;
    if(!mysessionvalue){
        res.redirect('/');
    }
    AdminRegistrationModel.findById(req.params.id,function(err,db_admin_array){
        if(err)
        console.log("Error");
        else
        {
            console.log(db_admin_array);
            res.render('Admin/Admin_edit',{admin_array:db_admin_array});
        }
    });
});
app.post('/admin/edit/:id',function(req,res){
    console.log("Edit ID is"+req.params.id);
    const admindata={
        Admin_id:           req.body.Admin_id,
        Admin_name:         req.body.Admin_name,
        Admin_contact_no:   req.body.Admin_contact_no,
        Admin_email:        req.body.Admin_email,
        Admin_password:     req.body.Admin_password
    }
    AdminRegistrationModel.findByIdAndUpdate(req.params.id,admindata,function(err){
            if(err)
            {
            console.log(req.params.id);
                console.log("Error in Record Update");
            }
        else
            res.redirect('/admin/data_display');
        });
    });    
//edit data

//single-record
app.get('/admin/show/:id',function(req,res){
    var mysessionvalue= req.session.Admin_email;
    if(!mysessionvalue){
        res.redirect('/');
    }
    console.log(req.params.id);
    AdminRegistrationModel.findById(req.params.id,function(err,db_admin_array){
        if(err)
        console.log("Error in single Record Fetch");
        else
        {
            console.log(db_admin_array);
            res.render('Admin/Admin_singledata',{admin_array:db_admin_array});
        }
    });
});


/* GET home page. */
app.get('/Admin_login', function(req, res, next) {
  AdminLoginModel.find(function(err,db_admin_array){
  if(err)
  {
console.log(err);
  }
  else{
    res.render('Admin_login',{admin_array:db_admin_array});
  }
  });
});


app.post('/login_process',function(req,res,next){

  //alert("here1");
  
  AdminLoginModel.find(function(err,db_admin_array){
    if(err){     

        //alert("here1");
  
        console.log(err);
    }
    else{

            //alert("here1");

             console.log(req);

             console.log(db_admin_array);
  
      
              var flag=true;  //temp variable
              var admin_id=req.body.Admin_id;
              var admin_email=req.body.Admin_email;
              var admin_password = req.body.Admin_password;

              //var admin_email="aa2@gmail.com";
              //var admin_password = "12345";
              
              req.session.Admin_email = admin_email;
              req.session.Admin_password=admin_password;
              
              console.log("Email"+req.session.Admin_email);
              console.log("Password"+req.session.Admin_password);
            
              if(db_admin_array.length>0)
              {
                for(var i=0;i<db_admin_array.length;i++)
                {
                    if( req.session.Admin_email  ==  db_admin_array[i].Admin_email && 
                        req.session.Admin_password  ==  db_admin_array[i].Admin_password)
                      {
                        flag=true;
                        console.log("Session Connected Successfully");
                        res.redirect('/admin/data_display');
                        break;
                      }
                      else{
                        flag=false;  
                      } 
                }
                if(!flag)
                {
                    res.redirect('/');
                }  
              }
            else
            {
              res.redirect('/'); 
            }

            
        }
  });
});

app.get('/logout', function(req, res, next) {
  req.session.destroy();
  
  res.redirect('/');
});

app.get('/forgotpassword', function(req, res, next) {
  res.render('forgot_password');
});




app.get('/upload', function(req,res,next){
res.render('upload-file',{title:'Upload File', success:''})

 });

 
//for image




//Add Investing
app.get('/investing/add', function(req, res, next) {
    res.render('Investing/Investing_add');
});


app.post('/investing/add',function(req,res,next){
    console.log(req.body);
    const investing_data={
         video           :     req.body.video,
        description           :     req.body.description,
        description2     :     req.body.description2,
        consistenttxt1           :     req.body.ctxt1 ,
        consistenttxt2           :     req.body.ctxt2 ,
        sustaintxt1           :     req.body.stxt1 ,
        sustaintxt2           :     req.body.stxt2 ,
        robusttxt1           :     req.body.rtxt1 ,
        robusttxt2         :     req.body.rtxt2 ,
        image_upload      :     req.body.image
    }
    var investingdata=InvestingModel(investing_data);  
    investingdata.save(function(err){
        if(err)
        console.log(err);
        else
        res.redirect('/investing/add');
        });
    });
    
    //display data
    app.get('/investing/data_display',function(req,res,next){
        var mysessionvalue= req.session.Admin_email;
        //var mysessionvalue= "aa2@gmail.com";
        if(!mysessionvalue){
            res.redirect('/');
        } 
        InvestingModel.find(function(err,db_investing_array){
            if(err)
            console.log("Error");
            else
            {
            console.log("hello value :"+db_investing_array);
            res.render('Investing/Investing_display',{investing_array:db_investing_array});
            }
        });
    });
    //display data
    
    //delete data
    app.get('/investing/delete/:id',function(req,res){
        var mysessionvalue= req.session.Admin_email;
        if(!mysessionvalue){
            res.redirect('/');
        }
        InvestingModel.findByIdAndDelete(req.params.id,function(err,db_investing_array){
            if(err)
            console.log("Error in delete data");
            else
            {
                console.log(db_investing_array);
                res.redirect('/data_display');
            }
        });
    });
    //delete data
    //edit data
    app.get('/investing/edit/:id',function(req,res,next){
        var mysessionvalue= req.session.Admin_email;
        if(!mysessionvalue){
            res.redirect('/');
        }
        InvestingModel.findById(req.params.id,function(err,db_investing_array){
            if(err)
            console.log("Error");
            else
            {
                console.log("before edit : ",db_investing_array);
                res.render('Investing/Investing_edit',{investing_array:db_investing_array});
            }
        });
    });
    app.post('/investing/edit/:id',function(req,res){
        console.log("Edit ID is"+req.params.id);
            const investing_data= {
                video           :     req.body.video,
                description           :     req.body.description,
                description2     :     req.body.description2,
                consistenttxt1           :     req.body.ctxt1 ,
                consistenttxt2           :     req.body.ctxt2 ,
                sustaintxt1           :     req.body.stxt1 ,
                sustaintxt2           :     req.body.stxt2 ,
                robusttxt1           :     req.body.rtxt1 ,
                robusttxt2         :     req.body.rtxt2 ,
                image_upload      :     req.body.image
            }
        InvestingModel.findByIdAndUpdate(req.params.id,investing_data,function(err){
                if(err)
                {
                console.log(req.params.id);
                    console.log("Error in Record Update");
                }
            else
                res.redirect('/data_display');
            });
        });    
    //edit data
    //single-record
    app.get('/investing/show/:id',function(req,res){
        var mysessionvalue= req.session.Admin_email;
        if(!mysessionvalue){
            res.redirect('/');
        }
        console.log(req.params.id);
        InvestingModel.findById(req.params.id,function(err,db_investing_array){
            if(err)
            console.log("Error in single Record Fetch");
            else
            {
                console.log(db_investing_array);
                res.render('Investing/Investing_singledata',{investing_array:db_investing_array});
            }
        })
    })

    var getInvest =  InvestingModel.find();


app.get('/api/investing/data_display',function(req, res, next) {
  getInvest.exec(function(err,data){
  if(err) throw err;
  res.send(data);
    });
    
  });

 var getAdmin =  AdminRegistrationModel.find();

app.get('/api/admin/data_display',function(req, res, next) {
  getAdmin.exec(function(err,data){
  if(err) throw err;
  res.send(data);
    });
    
  });



   


app.listen(4201, (req, res) => {
  console.log('The server is up and running on port 4201')
});